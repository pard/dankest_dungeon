rltk::add_wasm_support!();
use rltk::{Console, GameState, Rltk, RGB, VirtualKeyCode};
use specs::prelude::*;
use std::cmp::{max, min};

#[macro_use]
extern crate specs_derive;

struct State {
    ecs: World,
}

#[derive(Component)]
struct Position {
    x: i32,
    y: i32,
}

#[derive(Component)]
struct Renderable {
    glyph: u8,
    fg: RGB,
    bg: RGB,
}


impl GameState for State {
    fn tick(&mut self, ctx: &mut Rltk) {
        ctx.cls();
        ctx.print(1, 1, "Hello world!");
    }
}

fn main() {
    let context = Rltk::init_simple8x8(
        80, 50, "Hello world!", "resources"
    );

    let gs = State{
        ecs: World::new()
    };
    gs.ecs.register::<Position>;
    gs.ecs.register::<Renderable>();

    gs.ecs
        .create_entity()
        .with(Position {x: 40, y: 25 })
        .with(Renderable {
            glyph: rltk::to_cp437('@'),
            fg: RGB::named(rltk::YELLOW),
            bg: RGB::named(rltk::BLACK),
        })
        .build();

    for i in 0..10 {
        gs.ecs
        .create_entity()
        .with(Position {x: i * 7, y: 20 })
        .with(Renderable {
            glyph: rltk::to_cp437('g'),
            fg: RGB::named(rltk::GREEN),
            bg: RGB::named(rltk::BLACK),
        })
        .build();
    }

    rltk::main_loop(context, gs);
}
